package com.morya.users_service.controller;

import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users-service")
public class UsersServiceController {

	@GetMapping("/hello")
	public String getHelloMsg() {
		return "Hello from UsersService " + new Date();
	}
}
